           08  FILE-SYSTEM                  PIC X(10).
           08  FM-KEY.                                                  00000500
DE005          10  FM-PLAN-NUM              PIC X(06).                  00000600
DE006          10  FM-PLAN-SEQ              PIC -9(3).                  00000700
DE007          10  FM-PART-ID.                                          00000800
DE008              15  FM-PART-NUM          PIC 9(09).                  00000900
DE009              15  FM-PART-SUB-PLAN     PIC X(06).                  00001200
DE010              15  FM-PART-EXT          PIC X(02).                  00001300
DE015          10  FM-DE-TYPE               PIC X(02).                  00001400
DE020          10  FM-KEY-DATA              PIC X(40).                  00003400
DE025          10  FM-DATE                  PIC S9(8).                  00007200
DE030          10  FM-DENUM                 PIC 9(3).                   00007300
DE035          10  FM-SEQ                   PIC -9(3).                  00007400
               10  FM-KEY-FILLER            PIC X(21).                  00007500
               10  FM-REC-TYPE              PIC X(02).                  00007600
           08  FM-DATA.                                                 00007800
DE100          10  FM-TRAN-CODE             PIC X(03).                  00007900
DE110          10  FM-OLD-DE-VALUE          PIC X(80).                  00008000
DE120          10  FM-NEW-DE-VALUE          PIC X(80).                  00008100
DE130          10  FM-USER-ID               PIC X(20).                  00008300
DE140          10  FM-RUN-DATE              PIC  9(08).                 00008400
DE150          10  FM-RUN-TIME              PIC -9(06).                 00008500
14312S         10  FM-DATA-FILLER           PIC X(71).                  00008600


