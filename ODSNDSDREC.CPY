     *******************************************************            00000100
     ***  NOTE DETAIL RECORD                     95/05   ***            00000200
     *******************************************************            00000300
           10  ND-KEY.                                                  00000400
DE006          15  ND-PLAN-NUM.                                         00000500
                   20  ND-PLAN-PREFIX      PIC  X.                      00000600
                   20  ND-PLAN-SUFFIX      PIC  X(5).                   00000700
DE007          15  ND-PLAN-SEQ             PIC -9(3).                   00000800
DE010          15  ND-PART-ID.                                          00000900
DE012              20  ND-PART-NUM         PIC  9(9).                   00001000
                   20  ND-PART-NUM-X REDEFINES ND-PART-NUM              00001100
                                           PIC  X(9).                   00001200
DE014              20  ND-PART-SUB-PLAN    PIC  X(6).                   00001300
DE016              20  ND-PART-EXT.                                     00001400
                       25  ND-PART-EXT-1   PIC  X(1).                   00001500
                       25  ND-PART-EXT-2   PIC  X(1).                   00001600
DE018          15  ND-DATE                 PIC -9(9).
DE020          15  ND-SEQ                  PIC -9(3).
DE022          15  ND-DETAIL-SEQ           PIC -9(4).9(9).
               15  ND-FILLER               PIC  X(05).                  00002000
DE027          15  ND-REC-TYPE             PIC  X(2).                   00002100
           10  ND-DATA.                                                 00002200
DE100          15  ND-LINE                 PIC  X(72).                  00002300



